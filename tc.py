import time
start_time=time.time()
fname=open("abc.txt","r")

def wordscount(file):
    f = open(file, 'r')
    count = 0
    for line in f:
        words = line.split()
        count = count + len(words)
    return count

def characterscount(file):
    f = open(file, 'r')
    count = 0
    for line in f:
        for char in line:
            count = count + 1
    return count
#read the content of file and replace spaces with nothing
data = fname.read().replace(" ","")
#get the length of the data
number_of_characters = len(data)

print('The file has:')
print('Number of words:   ', wordscount('abc.txt'))
print('Number of characters in text file without space :', number_of_characters)
print('Number of characters in text file with space :', characterscount('abc.txt'))
print("start time is:",start_time)
end_time=time.time()
print("end time is:",end_time)
print("execution time is:",end_time-start_time)